function mostDismissalsOfPlayerByAnother(matchesData, dataMatchDeliveries) {
  let result = {};
  for (let index = 0; index < dataMatchDeliveries.length; index++) {
    let dismissalKind = dataMatchDeliveries[index].dismissal_kind;
    let currentBatsman = dataMatchDeliveries[index].batsman;
    let currentBowler = dataMatchDeliveries[index].bowler;

    if (dismissalKind) {
      if (result[currentBatsman]) {
        if (result[currentBatsman][currentBowler]) {
          result[currentBatsman][currentBowler]++;
        } else {
          result[currentBatsman][currentBowler] = 1;
        }
      } else {
        result[currentBatsman] = {};
        result[currentBatsman][currentBowler] = 1;
      }
    }
  }

  let resultArray = Object.entries(result);

  for (let item of resultArray) {
    const sorted = Object.entries(item[1])
      .sort(([, item1], [, item2]) => item2 - item1)
      .slice(0, 1)
      .reduce(
        (item, [key, value]) => ({
          ...item,
          [key]: value,
        }),
        {}
      );
    result[item[0]] = sorted;
  }
  return result;
}

const csv = require("csv-parser");
const fs = require("fs");
const { parse } = require("path");
const matchesData = [];
const deliveriesData = [];
fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => matchesData.push(data))
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv({ separator: "," }))
      .on("data", (data) => deliveriesData.push(data))
      .on("end", () => {
        let resultObj = mostDismissalsOfPlayerByAnother(
          matchesData,
          deliveriesData
        );
        const content = JSON.stringify(resultObj, null, 2);
        fs.writeFileSync(
          "../public/output/8-most-dismissals-of-a-player-by-another.json",
          content,
          function (err) {
            if (err) {
              console.log(err);
            }
          }
        );
      });
  });
