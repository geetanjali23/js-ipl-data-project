function playerOfTheMatchOfEachSeason(matchedData) {
  let result = {};
  for (let index = 0; index < matchedData.length; index++) {
    let year = matchedData[index].season;
    let playerOfMatchName = matchedData[index].player_of_match;
    if (result[year]) {
      if (result[year][playerOfMatchName]) {
        result[year][playerOfMatchName]++;
      } else {
        result[year][playerOfMatchName] = 1;
      }
    } else {
      result[year] = {};
      result[year][playerOfMatchName] = 1;
    }
  }
  let resultFinal = {};
  let resultArray = Object.entries(result);
  for (let item of resultArray) {
    let max = 0;
    let maxKey = "";
    for (let entries in item[1]) {
      if (item[1][entries] > max) {
        max = item[1][entries];
        maxKey = entries;
      }
    }
    resultFinal[item[0]] = {};
    resultFinal[item[0]][maxKey] = max;
  }
  return resultFinal;
}

const csv = require("csv-parser");
const fs = require("fs");
const { parse } = require("path");
const matchesData = [];
const deliveriesData = [];
fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => matchesData.push(data))
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv({ separator: "," }))
      .on("data", (data) => deliveriesData.push(data))
      .on("end", () => {
        let resultObj = playerOfTheMatchOfEachSeason(
          matchesData,
          deliveriesData
        );
        const content = JSON.stringify(resultObj, null, 2);
        fs.writeFileSync(
          "../public/output/6-highest-number-of-Player-of-the-match.json",
          content,
          function (err) {
            if (err) {
              console.log(err);
            }
          }
        );
      });
  });
