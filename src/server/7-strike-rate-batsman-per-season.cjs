function strikeRateBatsmanPerSeason(matchesData, matchdeliveriesData) {
  let matchIdsObj = {};
  for (let index = 0; index < matchesData.length; index++) {
    let year = matchesData[index].season;
    let id = matchesData[index].id;
    matchIdsObj[id] = year;
  }
  let batsmanRunsAndBalls = matchdeliveriesData.reduce((batsmanData, delivery) => {
    let matchId = delivery.match_id;
    let batsman = delivery.batsman;
    let batsmanTotalRuns = parseInt(delivery.total_runs);

    if (batsmanData.hasOwnProperty(batsman)) {
      if (batsmanData[batsman][matchIdsObj[matchId]]) {
        batsmanData[batsman][matchIdsObj[matchId]].runs += batsmanTotalRuns;
        batsmanData[batsman][matchIdsObj[matchId]].balls++;
      }
      else {
        batsmanData[batsman][matchIdsObj[matchId]] = { runs: batsmanTotalRuns, balls: 1 };
      }
    }
    else {
      batsmanData[batsman] = {};
      if (matchIdsObj.hasOwnProperty(matchId)) {
        batsmanData[batsman][matchIdsObj[matchId]] = { runs: batsmanTotalRuns, balls: 1 };
      }
    }
    return batsmanData;
  }, {});
  let batsmanStrikeRate = {};
  let strikeRateFration;
  for (let batsman of Object.keys(batsmanRunsAndBalls)) {
    batsmanStrikeRate[batsman] = {};
    for (let year of Object.keys(batsmanRunsAndBalls[batsman])) {
      strikeRateFration = (100 * (batsmanRunsAndBalls[batsman][year].runs / batsmanRunsAndBalls[batsman][year].balls));
      batsmanStrikeRate[batsman][year] = parseFloat((strikeRateFration).toFixed(2));
    }
  }
  return batsmanStrikeRate;
}
const csv = require("csv-parser");
const fs = require("fs");
const { parse } = require("path");
const matchesData = [];
const deliveriesData = [];
fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => matchesData.push(data))
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv({ separator: "," }))
      .on("data", (data) => deliveriesData.push(data))
      .on("end", () => {
        let resultObj = strikeRateBatsmanPerSeason(matchesData, deliveriesData);
        const content = JSON.stringify(resultObj, null, 2);
        fs.writeFileSync(
          "../public/output/7-strike-rate-batsman-per-season.json",
          content,
          function (err) {
            if (err) {
              console.log(err);
            }
          }
        );
      });
  });
