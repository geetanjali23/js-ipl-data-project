function matchesPlayedPerYearForAllTheYears(matchesData) {
  let resultObj = {};
  for (let index = 0; index < matchesData.length; index++) {
    let year = matchesData[index].season;
    if (resultObj[year]) {
      resultObj[year] = resultObj[year] + 1;
    } else {
      resultObj[year] = 1;
    }
  }
  return resultObj;
}
const csv = require("csv-parser");
const fs = require("fs");
const matchesData = [];
fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => matchesData.push(data))
  .on("end", () => {
    let resultObj = matchesPlayedPerYearForAllTheYears(matchesData);
    const content = JSON.stringify(resultObj, null, 2);
    fs.writeFileSync(
      "../public/output/1-matches-per-year.json",
      content,
      function (err) {
        if (err) {
          console.log(err);
        }
      }
    );
  });
