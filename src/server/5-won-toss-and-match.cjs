// function teamWonTossPlusMatch(matchesData, deliveriesData) {
//   let eachTeamTossandmatchOwnData = matchesData.reduce((teamsDataObj, eachMatch) => {
//     if (eachMatch.toss_winner == eachMatch.winner) {
//       if (teamsDataObj[eachMatch.winner]) {
//         teamsDataObj[eachMatch.winner] += 1;
//       } else {
//         teamsDataObj[eachMatch.winner] = 1;
//       }
//     }
//     return teamsDataObj;
//   }, {});
//   return eachTeamTossandmatchOwnData;
// }
function teamWonTossPlusMatch(matchesData, deliveriesData) {
  let result = {};
  for (let index = 0; index < matchesData.length; index++) {
    let tossWinnerTeam = matchesData[index].toss_winner;
    let winnerTeam = matchesData[index].winner;
    if (result.hasOwnProperty(tossWinnerTeam)) {
      if (tossWinnerTeam === winnerTeam) {
        result[tossWinnerTeam]++;
      }

    }
    else {
      result[tossWinnerTeam] = 0;
      if (tossWinnerTeam === winnerTeam) {
        result[tossWinnerTeam] = 1;
      }
    }
  }
  return result;
}
const csv = require("csv-parser");
const fs = require("fs");
const { parse } = require("path");
const matchesData = [];
const deliveriesData = [];
fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => matchesData.push(data))
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv({ separator: "," }))
      .on("data", (data) => deliveriesData.push(data))
      .on("end", () => {
        let resultObj = teamWonTossPlusMatch(
          matchesData,
          deliveriesData
        );
        const content = JSON.stringify(resultObj, null, 2);
        fs.writeFileSync(
          "../public/output/5-won-toss-and-match.json",
          content,
          function (err) {
            if (err) {
              console.log(err);
            }
          }
        );
      });
  });




