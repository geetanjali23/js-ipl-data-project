// function extraRunsConcededPerTeamIn2016(matchesData, deliveriesData) {
//   let matchIdSet = new Set();

//   for (let index = 0; index < matchesData.length; index++) {
//     if (matchesData[index].season == "2016") {
//       const matchId = matchesData[index].id;
//       matchIdSet.add(matchId);
//     }
//   }
//   let teamsConcededRuns = {};
//   for (let index = 0; index < deliveriesData.length; index++) {
//     let matchId = deliveriesData[index].match_id;

//     if (matchIdSet.has(matchId)) {
//       let bowlingTeam = deliveriesData[index].bowling_team;
//       let extraRuns = Number(deliveriesData[index].extra_runs);

//       if (extraRuns > 0) {
//         if (teamsConcededRuns.hasOwnProperty(bowlingTeam)) {
//           teamsConcededRuns[bowlingTeam] =
//             teamsConcededRuns[bowlingTeam] + extraRuns;
//         } else {
//           teamsConcededRuns[bowlingTeam] = extraRuns;
//         }
//       }
//     }
//   }
//   return teamsConcededRuns;
// }

function extraRunsConcededPerTeamIn2016(matchesData, deliveriesData) {
  let matchIdFromMatchesData = [];
  const result = {};
  for (let index = 0; index < matchesData.length; index++) {
    if (matchesData[index].season === "2016") {
      matchIdFromMatchesData.push(matchesData[index].id);
    }
  }
  for (let index = 0; index < deliveriesData.length; index++) {
    let deliveyDataObjIdx = deliveriesData[index];
    let bowlingTeam = deliveyDataObjIdx.bowling_team;
    if (matchIdFromMatchesData.includes(deliveyDataObjIdx.match_id)) {
      if (result[bowlingTeam]) {
        result[bowlingTeam] = result[bowlingTeam] + Number(deliveyDataObjIdx.extra_runs);
      }
      else {
        result[bowlingTeam] = Number(deliveyDataObjIdx.extra_runs);
      }
    }
  }
  return result;
}
const csv = require("csv-parser");
const fs = require("fs");
const matchesData = [];
const deliveriesData = [];
fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => matchesData.push(data))
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv({ separator: "," }))
      .on("data", (data) => deliveriesData.push(data))
      .on("end", () => {
        let resultObj = extraRunsConcededPerTeamIn2016(
          matchesData,
          deliveriesData
        );
        const content = JSON.stringify(resultObj, null, 2);
        fs.writeFileSync(
          "../public/output/3-extra-runs-conceded-per-team-2016.json",
          content,
          function (err) {
            if (err) {
              console.log(err);
            }
          }
        );
      });
  });

