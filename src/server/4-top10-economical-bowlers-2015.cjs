function top10EconomicalBowlersInTheYear2015(matchesData, deliveriesData) {
  let matchIdSet = new Set();
  for (let index = 0; index < matchesData.length; index++) {
    if (matchesData[index].season == "2015") {
      const matchesId = matchesData[index].id;
      matchIdSet.add(matchesId);
    }
  }
  let bowlersAndBalls = new Map();
  let bowlersAndConcededRuns = new Map();
  for (let index = 0; index < deliveriesData.length; index++) {
    let matchId = deliveriesData[index].match_id;
    if (matchIdSet.has(matchId)) {
      let bowlerName = deliveriesData[index].bowler;
      let runs = Number(deliveriesData[index].total_runs);
      let ball = deliveriesData[index].extra_runs == "0" ? 1 : 0;

      if (bowlersAndBalls.has(bowlerName)) {
        let ballsThrown = Number(bowlersAndBalls.get(bowlerName));
        ballsThrown += ball;
        bowlersAndBalls.set(bowlerName, ballsThrown);

        let totalRuns = bowlersAndConcededRuns.get(bowlerName);
        totalRuns += runs;
        bowlersAndConcededRuns.set(bowlerName, totalRuns);
      } else {
        bowlersAndBalls.set(bowlerName, ball);
        bowlersAndConcededRuns.set(bowlerName, runs);
      }
    }
  }

  let bowlersAndEconomy = new Map();
  bowlersAndBalls.forEach((value, key) => {
    let bowlerName = key;
    let balls = value;
    let overs = balls / 6;
    let runs = bowlersAndConcededRuns.get(bowlerName);
    let economy = runs / overs;

    bowlersAndEconomy.set(bowlerName, economy);
  })

  bowlersAndEconomy = new Map([...bowlersAndEconomy.entries()].sort((a, b) => a[1] - b[1]));
  let counter = 1;
  let top10EconomicalBowlers = {};
  for (const [key, value] of bowlersAndEconomy) {
    if (counter > 10)
      break;
      
    top10EconomicalBowlers[key] = value;
    counter++;
  }
  return top10EconomicalBowlers;
}

const csv = require("csv-parser");
const fs = require("fs");
const { parse } = require("path");
const matchesData = [];
const deliveriesData = [];
fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => matchesData.push(data))
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv({ separator: "," }))
      .on("data", (data) => deliveriesData.push(data))
      .on("end", () => {
        let resultObj = top10EconomicalBowlersInTheYear2015(
          matchesData,
          deliveriesData
        );
        const content = JSON.stringify(resultObj, null, 2);
        fs.writeFileSync(
          "../public/output/4-top10-economical-bowlers-2015.json",
          content,
          function (err) {
            if (err) {
              console.log(err);
            }
          }
        );
      });
  });



