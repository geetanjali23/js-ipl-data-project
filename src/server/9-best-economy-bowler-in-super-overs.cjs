function bestEconomyBowlerInSuperOvers(parsedDataMatches, parsedDataDelivery) {
  let result = {};
  for (let index = 0; index < parsedDataDelivery.length; index++) {
    let currentValueDelivery = parsedDataDelivery[index];
    let currentBowler = parsedDataDelivery[index].bowler;

    if (Number(currentValueDelivery.is_super_over)) {
      if (result[currentBowler]) {
        result[currentBowler][0] =
          Number(result[currentBowler][0]) +
          Number(currentValueDelivery.total_runs);
        result[currentBowler][1]++;
      } else {
        result[currentBowler] = [];
        result[currentBowler][0] = Number(currentValueDelivery.total_runs);
        result[currentBowler][1] = 1;
      }
    }
  }
  for (let item in result) {
    result[item] = result[item][0] / (result[item][1] / 6);
  }
  const sorted = Object.entries(result)
    .sort(([, a], [, b]) => a - b)
    .slice(0, 1)
    .reduce(
      (item, [key, value]) => ({
        ...item,
        [key]: value,
      }),
      {}
    );
  return result;
}
const csv = require("csv-parser");
const fs = require("fs");
const { parse } = require("path");
const matchesData = [];
const deliveriesData = [];
fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => matchesData.push(data))
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv({ separator: "," }))
      .on("data", (data) => deliveriesData.push(data))
      .on("end", () => {
        let resultObj = bestEconomyBowlerInSuperOvers(
          matchesData,
          deliveriesData
        );
        const content = JSON.stringify(resultObj, null, 2);
        fs.writeFileSync(
          "../public/output/9-best-economy-bowler-in-super-overs.json",
          content,
          function (err) {
            if (err) {
              console.log(err);
            }
          }
        );
      });
  });
