function matchesWonperTeamPerYear(matchesData) {
  let perTeamPerYearObj = {};

  for (let index = 0; index < matchesData.length; index++) {
    let season = matchesData[index].season;
    let winner = matchesData[index].winner;
    if (matchesData[index].winner.length > 1) {
      if (perTeamPerYearObj[winner]) {
        if (perTeamPerYearObj[winner][season]) {
          perTeamPerYearObj[winner][season]++;
        } else {
          perTeamPerYearObj[winner][season] = 1;
        }
      } else {
        perTeamPerYearObj[winner] = {};
        perTeamPerYearObj[winner][season] = 1;
      }
    }
  }
  return perTeamPerYearObj;
}
const csv = require("csv-parser");
const fs = require("fs");
const matchesData = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv({separator: ','}))
  .on("data", (data) => matchesData.push(data))
  .on("end", () => {
    let resultObj = matchesWonperTeamPerYear(matchesData);

    const content = JSON.stringify(resultObj, null, 2);
    fs.writeFileSync(
      "../public/output/2-matches-won-per-team-per-year.json",
      content,
      function (err) {
        if (err) {
          console.log(err);
        }
      }
    );
  });
